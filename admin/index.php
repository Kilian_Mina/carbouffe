<?php
session_start();
if ((!isset($_SESSION['modadmin'])) && (!isset($_SESSION['IdClient']))) {
    $_SESSION['modadmin'] = 'Visitor';
}
include("../modeles/cbbdd.php");
$thePdo = cb::getPdocb();
?>
<!DOCTYPE html>

<html>
<head>
    <!-- //Main -->
    <meta name="description" content="Adlmin Carbouffe">
    <meta name="author" content="Kilian Mina - Léa Cotté">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <!-- Css -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../fonts/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/date.css"/>
    <link rel="stylesheet" href="../css/date2.css"/>


    <title>CarBouffe - Admin</title>
</head>


<body>
<div id="wrapper">
    <div id="sidebar-wrapper">
        <?php
        //Header

        if ($_SESSION['modadmin'] == 'Admin') {
            include("vues/menu_conect.php");
        } else if (isset($_SESSION['modadmin'])) {
            include("vues/menu.php");
        }

        ?>

    </div>
    <hr>

    <div class="container-fluid text-center">

        <?php

        if ($_SESSION['modadmin'] == 'Admin') {
            include("vues/buleprincipale.php");
        }

        //Récupérer UC
        if (!isset($_REQUEST['uc']))
            $uc = 'login';
        else
            //Récupére r UC
            if (!isset($_REQUEST['uc']))
                $uc = 'login';
            else
                $uc = $_REQUEST['uc'];


        switch ($uc) {
            //vues/user
            case 'login':
                {
                    include("vues/login.php");
                    break;
                }
            case 'accueil':
                {
                    include("vues/accueil.php");
                    break;
                }
            case 'disco':
                {
                    $_SESSION['modadmin'] = 'visitor';
                    header('Refresh:0 ; url=index.php');
                }

            case 'produit':
                {
                    include("controllers/c_admin_produits.php");
                    break;
                }
            case 'categorieproduit':
                {
                    include("controllers/c_admin_categorie_produits.php");
                    break;
                }
            case 'client':
                {
                    include("controllers/c_admin_client.php");
                    break;
                }
        }
        ?>
    </div>
</div>

<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/popper/popper.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/jq_date.js"></script>
<script type="text/javascript" src="../js/date.js"></script>
<script type="text/javascript" src="../js/date2.js"></script>
</body>
</html>


