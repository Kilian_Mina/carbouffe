<div class="container">

</div>

</br>


<div class="col-md-10 col-sm-10 container">
    <h2>Catégories Produits</h2>
    <button type="button" class="btn btn-success btn-lg btn-block"
            onclick="document.location.href = 'index.php?uc=categorieproduit&co=afficheajouter'">Ajouter
    </button>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Nom</th>
                    <th>Description</th>

                </tr>
            </thead>
            <tbody>
            <?php while ($categorieproduit = $CategorieProduits->fetch()) { ?>
                    <tr>
                        <td><img class="rounded-top"
                                 src="../images/categorie_produits/categ<?php echo $categorieproduit['IdCategorie']; ?>.jpg"
                                 alt="<?php echo addslashes(str_replace(' ', '', $categorieproduit['NomCategorie'])); ?>"
                                 style="max-width:200px;"/></td>
                        <td><?php echo $categorieproduit['NomCategorie'] ?></td>
                        <td><?php echo $categorieproduit['DescriptionCategorie'] ?></td>
                        <td>
                            <i class="fa fa-window-close fa-3x" style="color:red" aria-hidden="true" onclick="if (confirm('Etes vous sûr de vouloir continuer ? cette action sera irreversible ! ')) {
                                    document.location.href = 'index.php?uc=categorieproduit&co=supprimer&idprod=<?php echo $categorieproduit['IdCategorie'] ?>'
                                    }
                                    "><h5>Supprimer</h5></i>
                            <i class="fa fa-pencil fa-3x" style="color:orange"
                               onclick="document.location.href = 'index.php?uc=categorieproduit&co=affichemodifier&idprod=<?php echo $categorieproduit['IdCategorie']; ?>'"/>
                            <h5> Modifier</h5> </i>
                        </td>
                    </tr>
<?php } ?>
            </tbody>
        </table>
    </div>
</div>
</br></br></br></br>