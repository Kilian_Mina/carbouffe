<?php 
	if ($_SESSION['modadmin'] == 'Admin'){header('Refresh:0 ; url=index.php?uc=accueil');}
?>
<body>
    <div class="container col-md-3 offset-md-3 justify-content-center">
		<div class=" justify-content-center">
			<div class="card ">
				<div class="card-header">                                
					<div class="row-fluid user-row">
						<img src="../images/admin/logo_login_admin.png" class="img-fluid" alt="Conxole Admin"/>
						<p>Carbouffe - Administrateur</p>
					</div>
				</div>
				<div class="card-block">
					<form accept-charset="UTF-8" role="form" method="post" action="../controllers/c_authentification.php?statut=admin" class="form-signin">
						<fieldset>
							<label class="panel-login">
								<div class="login_result"></div>
							</label>
							<input class="form-control" placeholder="Email" name="mail" id="username" type="text">
							<input class="form-control" placeholder="Password" name="pass" id="password" type="password">
							
							<input class="btn btn-lg btn-success btn-block" type="submit" id="login" value="Login »">
						</fieldset>
					</form>
				</div>
			</div>
        </div>
    </div>
</body>
