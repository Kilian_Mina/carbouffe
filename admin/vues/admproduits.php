<div class="container">

</div>

</br>


<div class="col-md-10 col-sm-10 container">
	<h2>Produits</h2>
    <button type="button" class="btn btn-success btn-lg btn-block"
            onclick="document.location.href='index.php?uc=produit&co=afficheajouter'">Ajouter
    </button>
	<div class="table-responsive">
		<table class="table table-striped" >
			<thead>
				<tr>
					<th>Image</th>
					<th>Nom</th>
					<th>Qte en Stock</th>
					<th>Prix du Produit</th>
					<th>Réduction du produit</th>
					<th>Description</th>
				</tr>
			</thead>
			<tbody>
			<?php while($produit = $Produits->fetch()){ ?>
				<tr>
				  <td ><img class="rounded-top" src="../images/produits/<?php echo $produit['IdProduit']; ?>.png" alt="<?php echo $produit['NomProduit']; ?> " width="100"/></td>
				  <td><?php echo $produit['NomProduit'] ?></td>
				  <td><?php echo $produit['StockProduit'] ?></td>
				  <td><?php echo $produit['PrixProduit'] ?></td>
				  <td><?php if(!isset($produit['ReducProduit'])){echo "0";}else{ echo $produit['ReducProduit'];} ?></td>
				  <td><?php echo $produit['Descriptif'] ?></td>
				  <td>
					<i class="fa fa-window-close fa-3x" style="color:red" onclick="
					if(confirm('Etes vous sûr de vouloir continuer ? cette action sera irreversible !'))
					{
                            document.location.href='index.php?uc=produit&co=supprimer&idprod=<?php echo $produit['IdProduit']; ?>'
                            }"><h5>Supprimer</h5></i>
                      <i class="fa fa-pencil fa-3x" style="color:orange"
                         onclick="document.location.href='index.php?uc=produit&co=affichemodifier&idprod=<?php echo $produit['IdProduit']; ?>'"/>
                      <h5> Modifier</h5> </i>
				  </td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
</br></br></br></br>