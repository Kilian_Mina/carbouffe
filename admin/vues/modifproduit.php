<div class="container ">

    <form class="form-horizontal" role="form" name="signin" method="POST"
          action="index.php?uc=produit&co=actionmodifier&idprod=<?php echo $LeProduit['IdProduit']; ?>"
          enctype="multipart/form-data">
		<h2>Modification du nouveau produit n°<?php echo $LeProduit['IdProduit']; ?></h2>
		
		<div class="form-group">
			<label class="col-sm-4 control-label">Nom</label>
			<input type="text" Value="<?php echo $LeProduit['NomProduit']; ?>" name="nom" class="form-control"  required>
		</div>
		
		
		<div class="form-group">
			<label for="sel1">Produit Présent</label>
			<select class="form-control" id="sel1" name="categorie" >

                <option value="<?php echo $LeProduit['CategorieProduit']; ?>"
                        selected><?php echo $SaCategorie['NomCategorie']; ?> (séléctionné)
                </option>
				<option disabled>________________________________________________________________</option>
				<option disabled></option>
				<?php while($produit = $SelectProd->fetch()){ ?>
                    <option value="<?php echo $produit['Idcategorie']; ?>"><?php echo $produit['NomCategorie']; ?></option>
				<?php } ?>
			</select>
		</div>
		
		<div class="form-group">
			<label  class="col-sm-3 control-label">Stock</label>
			<input type="number" name="stock" Value="<?php echo $LeProduit['StockProduit']; ?>" class="form-control"  required>
		</div>
		
		<div class="form-group">
			<label  class="col-sm-3 control-label">Prix</label>
			<input type="number" Value="<?php echo $LeProduit['PrixProduit']; ?>" class="form-control" name="prix" required>
		</div>
		
		<div class="form-group">
			<label  class="col-sm-4 control-label">Réduction (%)</label>
            <input type="number" Value="<?php echo $LeProduit['ReductionProduit']; ?>" class="form-control"
                   name="reduction">
		</div>
		
		<div class="form-group">
			<label   class="col-sm-3 control-label">Description</label>
			<textarea  type="text" class="form-control" name="description" ><?php echo $LeProduit['Descriptif']; ?></textarea>
		</div>
		
		<div class="form-group">
			<label   class="col-sm-3 control-label">Image</label></br>
			<img class="rounded-top" src="../images/produits/<?php echo $LeProduit['IdProduit']; ?>.png" alt="<?php echo addslashes($LeProduit['NomProduit']); ?> " width="200"/>
			<input type="file" name="image">
		</div>
		
		<div class="form-group">
			<button type="submit" class="btn btn-primary btn-block" id="register" onclick="confirm('voulez-vous continuez ?');">Confirmez</button>
		</div>
	</form> <!-- /form -->
</div> <!-- ./container -->