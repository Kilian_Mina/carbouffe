<div class="container ">

    <form class="form-horizontal" role="form" name="signin" method="POST" action="index.php?uc=categorieproduit&co=actionajouter" enctype="multipart/form-data">
		<h2>Ajout d'une catégorie produit </h2>
		<div class="form-group">
			<label class="col-sm-3 control-label">Nom</label>
			<input type="text" name="nom" class="form-control" required>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Description</label>
			<textarea  type="text" class="form-control" rows="10" name="description"  required></textarea>
		</div>
		<div class="form-group">
			<label   class="col-sm-3 control-label">Image</label></br>
            <input type="file" name="image"/>
		</div>

		<div class="form-group">
			<button type="submit" class="btn btn-primary btn-block" id="register" onclick="confirm('voulez-vous continuez ?');">Confirmez</button>
		</div>
	</form> <!-- /form -->
</div> <!-- ./container -->