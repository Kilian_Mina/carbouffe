<div class="container ">

    <form class="form-horizontal" role="form" name="signin" method="POST"
          action="index.php?uc=produit&co=actionmodifier&idprod=<?php echo $LeClient['IdClient']; ?>"
          enctype="multipart/form-data">
        <h2>Modification du Client n°<?php echo $LeClient['IdClient']; ?> : <?php echo $LeClient['NomClient']; ?></h2>

        <!--
        <div class="form-group">
            <label class="col-sm-3 control-label">Image</label></br>
            <img class="rounded-top" src="../images/produits/<?php //echo $LeClient['IdProduit']; ?>.png"
                 alt="<?php //echo addslashes($LeClient['NomProduit']); ?> " width="200"/>
            <label class="custom-file">
                <input type="file" id="file" name="image" class="custom-file-input">
                <span class="custom-file-control"></span>
            </label>
        </div>
-->

        <div class="form-group">
            <label class="col-sm-4 control-label">Nom</label>
            <input type="text" Value="<?php echo $LeClient['NomClient']; ?>" name="nom" class="form-control" required>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Prénom</label>
            <input type="text" Value="<?php echo $LeClient['PrenomClient']; ?>" name="prenom" class="form-control"
                   required>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Adresse</label>
            <textarea type="text" class="form-control"
                      name="Adresse"><?php echo $LeClient['AdresseClient']; ?></textarea>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Code Postale</label>
            <input type="number" class="form-control" min="97400" max="97499"
                   name="codepostal" value="<?php echo $LeClient['CodePostalClient']; ?>"></input>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">E-mail</label>
            <input type="text" Value="<?php echo $LeClient['EmailClient']; ?>" name="email" class="form-control"
                   required>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Mot de passe</label>
            <input type="password" Value="<?php echo $LeClient['PassClient']; ?>" name="password" class="form-control"
                   required>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block" id="register"
                    onclick="confirm('voulez-vous continuez ?');">Confirmez
            </button>
        </div>
    </form> <!-- /form -->
</div> <!-- ./container -->