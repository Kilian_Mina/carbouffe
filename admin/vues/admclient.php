<div class="container">

</div>

</br>


<div class="col-md-10 col-sm-10 container">
    <h2>Produits</h2>
    <button type="button" class="btn btn-success btn-lg btn-block"
            onclick="document.location.href='index.php?uc=produit&co=afficheajouter&categ=produit'">Ajouter
    </button>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Nom</th>
                <th>Prénom</th>
                <th>E-mail</th>
                <th>Date de Naissance</th>
                <th>Adresse</th>
                <th>Code Postal</th>
            </tr>
            </thead>
            <tbody>
            <?php while ($client = $Clients->fetch()) { ?>
                <tr>
                    <td><?php echo $client['NomClient'] ?></td>
                    <td><?php echo $client['PrenomClient'] ?></td>
                    <td><?php echo $client['EmailClient'] ?></td>
                    <td><?php echo $client['DateNaissClient'] ?></td>
                    <td><?php echo $client['AdresseClient'] ?></td>
                    <td><?php echo $client['CodePostalClient'] ?></td>
                    <td>
                        <i class="fa fa-window-close fa-3x" style="color:red" onclick="
                                if(confirm('Etes vous sûr de vouloir continuer ? cette action sera irreversible !'))
                                {
                                document.location.href='index.php?uc=produit&co=supprimer&idprod=<?php echo $client['IdClient']; ?>'
                                }"><h5>Supprimer</h5></i>
                        <i class="fa fa-pencil fa-3x" style="color:orange"
                           onclick="document.location.href='index.php?uc=client&co=affichemodifier&idclient=<?php echo $client['IdClient']; ?>'"/>
                        <h5> Modifier</h5> </i>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
</br></br></br></br>