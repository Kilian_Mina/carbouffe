<div class="container ">

    <form class="form-horizontal" role="form" name="signin" method="POST" action="index.php?uc=produit&co=actionajouter"
          enctype="multipart/form-data">
		<h2>Ajout d'un produit</h2>
		
		<div class="form-group">
			<label class="col-sm-4 control-label">Nom</label>
			<input type="text" name="nom" class="form-control"  required>
		</div>
		
		<div class="form-group">
			<label for="sel1">Produit Présent</label>
			<select class="form-control" id="sel1" name="categorie" >

				<?php while($produit = $SelectProd->fetch()){ ?>
                    <option value="<?php echo $produit['IdCategorie']; ?>"><?php echo $produit['NomCategorie']; ?></option>
				<?php } ?>
			</select>
		</div>
		
		<div class="form-group">
			<label  class="col-sm-3 control-label">Stock</label>
			<input type="number" name="stock" class="form-control"  required>
		</div>
		
		<div class="form-group">
			<label  class="col-sm-3 control-label">Prix</label>
			<input type="number" class="form-control" name="prix" required>
		</div>
		
		<div class="form-group">
			<label  class="col-sm-4 control-label">Réduction (%)</label>
			<input type="number" class="form-control" name="reduction" >
		</div>

		<div class="form-group">
			<label   class="col-sm-3 control-label">Description</label>
			<textarea  type="text" class="form-control" name="description" ></textarea>
		</div>
        <div class="form-group">
            <label   class="col-sm-3 control-label">Image</label></br>
            <input type="file" name="image"/>
        </div>
		
		<div class="form-group">
			<button type="submit" class="btn btn-primary btn-block" id="register" onclick="confirm('voulez-vous continuez ?');">Confirmez</button>
		</div>
	</form> <!-- /form -->
</div> <!-- ./container -->