<div class="container ">

    <form class="form-horizontal" role="form" name="signin" method="POST"
          action="index.php?uc=categorieproduit&co=actionmodifier&idprod=<?php echo $LeProduit['IdCategorie']; ?>"
          enctype="multipart/form-data">
        <h2>Modification du produit n°<?php echo $LeProduit['IdCategorie']; ?></h2>
		<div class="form-group">
			<label class="col-sm-3 control-label">Nom</label>
            <input type="text" name="nom" Value="<?php echo $LeProduit['NomCategorie']; ?>" class="form-control"
                   required>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label">Description</label>
            <textarea type="text" class="form-control" rows="10" name="description"
                      required><?php echo $LeProduit['DescriptionCategorie']; ?></textarea>
		</div>
		
		<div class="form-group">
			<label   class="col-sm-3 control-label">Image</label></br>
            <img class="rounded-top"
                 src="../images/categorie_produits/categ<?php echo $LeProduit['IdCategorie']; ?>.jpg"
                 alt="<?php echo addslashes($LeProduit['NomCategorie']); ?> " width="200"/>

				<input type="file" name="image">
		</div>

		<div class="form-group">
			<button type="submit" class="btn btn-primary btn-block" id="register" onclick="confirm('voulez-vous continuez ?');">Confirmez</button>
		</div>
	</form> <!-- /form -->
</div> <!-- ./container -->