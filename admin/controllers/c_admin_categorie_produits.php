<?php

if (!isset($_REQUEST['co']))
    $co = 'accueil_categorie_produit';
else
    $co = $_REQUEST['co'];

switch ($co) {
    case 'accueil_categorie_produit':
        {
            $CategorieProduits = $thePdo->afficheTouteCategProd();
            include("vues/adm_accueil_categorie_produit.php");
            break;
        }

    //-------------------------Modif-----------------------------------//
    case 'affichemodifier':
        {
            $LeProduit = $thePdo->afficheCategorieProduitId($_REQUEST['idprod']);
            include("vues/modifcategorieproduit.php");
            break;
        }
    case 'actionmodifier':
        {
            $id = $_REQUEST['idprod'];
            $nom = $_POST['nom'];
            $Description = $_POST['description'];

            if ($_FILES["image"]["error"] == UPLOAD_ERR_OK) {
                $uploads_dir = "../images/categorie_produits";
                $tmp_name = $_FILES["image"]["tmp_name"];
                $name = "categ" . $id . ".jpg";
                $target_file = $uploads_dir . "/" . $name;
                if (file_exists($target_file)) {
                    unlink($target_file);
                }
                move_uploaded_file($tmp_name, $target_file);
            }
            $thePdo->modifcategorieproduit($id, $nom, $Description);
            break;
        }

    //------------------------Ajout--------------------------------//
    case 'afficheajouter':
        {
            include("vues/ajoutcategorieproduit.php");
            break;
        }
    case 'actionajouter':
        {
            $nom = $_POST['nom'];
            $Description = $_POST['description'];
            $thePdo->ajoutcategorieproduit($nom, $Description);

            $id = $thePdo->IdDeNouvelleCategorieProduit($nom, $Description);

            if ($_FILES["image"]["error"] == UPLOAD_ERR_OK) {
                $uploads_dir = "../images/categorie_produits";
                $tmp_name = $_FILES["image"]["tmp_name"];
                $name = "categ" . $id['IdCategorie'] . ".jpg";
                $target_file = $uploads_dir . "/" . $name;
                move_uploaded_file($tmp_name, $target_file);
            }
            break;
        }
    //------------------------Supprimer----------------------------//
    case 'supprimer':
        {
            $thePdo->supprcategprod($_REQUEST['idprod']);
            header('Refresh:0; url=index.php?uc=produit&co=categorieproduit');
            break;
        }
}
?>