<?php

if (!isset($_REQUEST['co']))
    $co = 'accueil_produits';
else
    $co = $_REQUEST['co'];

switch ($co) {
    case 'accueil_produits':
        {
            $Produits = $thePdo->afficheProduit();
            include("vues/admproduits.php");
            break;
        }

    //-------------------------Modif-----------------------------------//
    case 'affichemodifier':
        {
            $LeProduit = $thePdo->afficheProduitId($_REQUEST['idprod']);
            $SaCategorie = $thePdo->afficheCategDuProduitParId($_REQUEST['idprod']);
            $SelectProd = $thePdo->afficheTouteCategProd();
            include("vues/modifproduit.php");
            break;
        }
    case 'actionmodifier':
        {
            $id = $_REQUEST['idprod'];
            $nom = $_POST['nom'];
            $categorie = $_POST['categorie'];
            $stock = $_POST['stock'];
            $prix = $_POST['prix'];
            $reduction = $_POST['reduction'];
            $description = $_POST['description'];

            if ($_FILES["image"]["error"] == UPLOAD_ERR_OK) {
                $uploads_dir = "../images/produits";
                $tmp_name = $_FILES["image"]["tmp_name"];
                $name = $id . ".png";
                $target_file = $uploads_dir . "/" . $name;
                if (file_exists($target_file)) {
                    unlink($target_file);
                }
                move_uploaded_file($tmp_name, $target_file);
            }
            
            $thePdo->modifproduit($id, $nom, $categorie, $stock, $prix, $reduction, $description);
            break;
        }

    //------------------------Ajout--------------------------------//
    case 'afficheajouter':
        {
            $SelectProd = $thePdo->afficheTouteCategProd();
            include("vues/ajoutproduit.php");
            break;
        }
    case 'actionajouter':
        {
            $nom = $_POST['nom'];
            $categorie = $_POST['categorie'];
            $stock = $_POST['stock'];
            $prix = $_POST['prix'];
            $reduction = $_POST['reduction'];
            $description = $_POST['description'];
            $thePdo->ajoutproduit($nom, $categorie, $stock, $prix, $reduction, $description);

            $id = $thePdo->IdDuNouveauProduit($nom, $categorie, $stock, $prix);
            if ($_FILES["image"]["error"] == UPLOAD_ERR_OK) {
                $uploads_dir = "../images/produits";
                $tmp_name = $_FILES["image"]["tmp_name"];
                $name = $id['IdProduit'] . ".png";
                $target_file = $uploads_dir . "/" . $name;
                move_uploaded_file($tmp_name, $target_file);
            }
            break;
        }

    //------------------------Supprimer----------------------------//
    case 'supprimer':
        {
            $thePdo->Supprprod($_REQUEST['idprod']);
            header('Refresh:0; url=index.php?uc=produit&co=accueil_produits');
            break;
        }
}
?>