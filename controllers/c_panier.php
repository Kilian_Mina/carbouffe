<?php

$AllPanier1 = $thePdo->AfficheProdPanier();
$AllPanier2 = $thePdo->AfficheProdPanier();

if (!isset($_REQUEST['co']))
    $co = 'accueil_panier';
else
    $co = $_REQUEST['co'];

switch ($co) {
    case 'accueil_panier': {
            include('vues/panier.php');
            break;
        }
    case 'ajoutprod': {
            $thePdo->AjoutProdPanier($_SESSION['Client'], $_POST['idproduit'], $_POST['qteprod']);
            header('Refresh:0; url=' . $_SESSION['urlactuel']);
            break;
        }
    case 'supprprod': {
            $thePdo->EffaceProduitPanier($_REQUEST['idcmd']);
            header('Refresh:0; url=index.php?uc=panier');
            break;
        }
    case 'modifproduitcommande': {
            $thePdo->modifProduitPanier($_REQUEST['idcmd'], $_REQUEST['Qte']);
        }
    case 'payement': {
            $client = $thePdo->adrrclient($_SESSION['Client']);

            include('vues/payement.php');
            break;
        }
}
?>
