<?php
$_SESSION['urlactuel'] = $_SERVER['REQUEST_URI'];
?>

<div class="modal fade product_view" id="product_view">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                <h3 class="modal-title"><p id="NomProd"></p></h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 product_img">
                        <p id="ImageIdProd"></p>
                    </div>

                    <div class="col-md-6 product_content">
                        <h4> Id du produit : <p id="IdProd"></p></h4>
                        <hr/>
                        <h4> Prix : </h4><h3 class="cost"><span class="glyphicon glyphicon-usd"></span> <p id="ReducProd"></p> <small class="pre-cost"><span class="glyphicon glyphicon-usd"></span> <p id="PrixProd"></p></small></h3>
                        <hr/>
                        <form method="POST" action="index.php?uc=panier&co=ajoutprod">
                            <div class="row">
                                <h4> Quantité :
                                    <input type="number" id="MaxStock" name="qteprod" value="1" class="form-control"
                                           min="1" max="" autofocus required>
                                </h4>
                                <!-- end col -->
                            </div>
                            <div class="space-ten"></div>
                            <div class="btn-ground">                                
                                <input type="hidden" id="formIdProd" name="idproduit" value=""/>
                                <input type="submit" class="btn btn-primary" value="Ajouter au Panier"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>