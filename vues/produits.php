    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Nos produits 
        <small>Par catégorie</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Accueil</a>
        </li>
        <li class="breadcrumb-item active">Nos Produits</li>
      </ol>

      <div class="row">
	  
		<?php 
			while ($Categ = $AllCateg->fetch()){
		?>
	  
        <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item ">
          <div class="card h-100 d-flex justify-content-center">
            <a href="index.php?uc=produits&co=affprod&cat=<? php echo $Categ['NomCategorie'] ?>" ><img class="card-img-top" src="images/categorie_produits/categ<?php echo $Categ['IdCategorie'] ?>.jpg" alt="<?php echo $Categ['NomCategorie'] ?>"></a>
            <div class="card-body">
              <h4 class="card-title ">
                <a href="index.php?uc=produits&co=affprod&cat=<?php echo $Categ['NomCategorie'] ?>"><?php echo $Categ['NomCategorie'] ?></a>
              </h4>
              <p class="card-text"><?php echo $Categ['DescriptionCategorie'] ?></p>
            </div>
          </div>
        </div>
		
		<?php 
		}
		?>
      </div>
    </div>
    <!-- /.container -->
