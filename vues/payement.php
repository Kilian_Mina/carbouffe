<div class="container text-center">
<br/><br/><br/><br/>
	<div class="stepwizard">
		<div class="stepwizard-row setup-panel">
			<div class="stepwizard-step">
				<a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
				<p>Livraison</p>
			</div>
			<div class="stepwizard-step">
				<a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
				<p>Paiement</p>
			</div>
			<div class="stepwizard-step">
				<a href="#step-3" type="button" class="btn btn-default btn-circle" disabled >3</a>
				<p>Terminer</p>
			</div>
		</div>
	</div>
	<br/><br/><br/><br/>
	<form role="form">
		<div class=" setup-content" id="step-1">
			<div class="col-sm-12">
				
				<h3>Votre moyen de livraison</h3>
				<br/><br/>
				<fieldset class="form-group">
					<label class="control-label"><h5>Quel serait votre moyen ?</h5></label>
						<div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="radio" name="moyenlivraison" id="legendRadio2" value="2" onclick="document.getElementById('newaddresse').style.display='inline';document.getElementById('addresseactu').style.display='none';">
								Une nouvelle Adresse
							</label>
							<br/>
							<label class="form-check-label">
								<input class="form-check-input" type="radio" checked name="moyenlivraison" id="legendRadio3" value="3" onclick="document.getElementById('newaddresse').style.display='none';document.getElementById('addresseactu').style.display='none';">
								A notre point de vente
							</label>
							<br/>
							<label class="form-check-label">
								<input class="form-check-input" type="radio" name="moyenlivraison" id="legendRadio1" value="1"  onclick="document.getElementById('newaddresse').style.display='none';document.getElementById('addresseactu').style.display='inline';">
								A Votre Adresse Actuelle
							</label>
							
						</div>
				</fieldset>
				<br/>
				<div id="newaddresse" style="display: none;">
					<div class="form-group">
						<label class="control-label">Nom</label>
						<input  maxlength="100" type="text"  class="form-control" placeholder="Entrer le nom" />
					</div>
					<div class="form-group">
						<label class="control-label">Prénom</label>
						<input maxlength="100" type="text"  class="form-control" placeholder="Entrer le prénom" />
					</div>
					<div class="form-group">
						<label class="control-label">Adresse</label>
						<input maxlength="100" type="text"  class="form-control" placeholder="Entrer l'addresse" />
					</div>
					<div class="form-group">
						<label class="control-label">Code Postal</label>
						<input type="number" id="Adresse" value="97400 - 97499" class="form-control" min="97400" max="97499" name="codepostale" autofocus />
					</div>
					
				</div>
				<div id="addresseactu" style="display: none;">
					<div class="form-group">
						<label class="control-label">Adresse</label>
						<input maxlength="100" type="text" disabled  class="form-control" placeholder="<?php echo $client['Adresse_Client']; ?>" />
					</div>
					<div class="form-group">
						<label class="control-label">Code Postal</label>
						<input disabled type="number" id="Adresse" value="<?php echo $client['CdePostal_Client']; ?>" class="form-control" min="97400" max="97499" name="codepostale"  />
					</div>
				</div>
				
			</div>
			<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Suivant</button>
		</div>
		<div class=" setup-content" id="step-2">
			
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-5 offset-md-4">
						<div class="card ">
							<div class="card-header">
								<div class="row">
									<h3 class="text-xs-center">Détails du Paiement</h3>
									<img class="img-fluid cc-img" src="images/card.png">
								</div>
							</div>
							<div class="card-block">
								<form role="form">
									<div class="row">
										<div class="col-xs-12" style="margin-left:20px;">
											<div class="form-group">
												<label>NUMERO DE LA CARTE</label>
												<div class="input-group">
													<input type="tel" class="form-control" placeholder="Valid Card Number" required />
													<span class="input-group-addon"><span class="fa fa-credit-card"></span></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-7 col-md-7" >
											<div class="form-group">
												<label><span class="hidden-xs">DATE D'EXPIRATION</span><span class="visible-xs-inline">EXP</span> DATE</label>
												<input type="tel" class="form-control" placeholder="MM / AA" required />
											</div>
										</div>
										<div class="col-xs-5 col-md-5 float-xs-right">
											<div class="form-group">
												<label>CB CODE</label>
												<input type="tel" class="form-control" placeholder="CVC" required />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12" style="margin-left:20px;">
											<div class="form-group">
												<label>CRYPTOGRAMME</label>
												<input type="text" class="form-control" placeholder="Card Owner Names" required />
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>	
					</div>
		
					<div class="col-xs-12 col-md-5 offset-md-4">
						<img class="pp-img" src="images/paypal.png" alt="Image Alternative text" title="Image Title">
						<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Accès à votre compte paypal</button>
					</div>
					
				</div>
				<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Suivant</button>
			</div>

			<style>
				.cc-img {
					margin: 0 auto;
				}
			</style>
			
		</div>	
		<div class=" setup-content" id="step-3">
			<div class="col-xs-12">
				
					<h3> Terminer</h3>
					<button class="btn btn-success btn-lg" type="submit">Cliquer pour Finir!</button>
				
			</div>
		</div>
	</form>
</div>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>