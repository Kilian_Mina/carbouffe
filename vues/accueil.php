<header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
		  <?php 
			
		  $nouveaute = $Lesnouveaute->fetch()?>
			  <div onclick="document.location.href = 'index.php?uc=produits&co=newprod&prodid=<?php echo $nouveaute['IdProduit']?>'" class="carousel-item active" style="background-image: url('images/nouveau_produits/pub<?php echo $nouveaute['IdProduit']?>.jpg')">
				<div class="carousel-caption d-none d-md-block">
				
				  <p></p>
				</div>
			  </div>

		  <?php while ($nouveaute = $Lesnouveaute->fetch()){?>
          <!-- Slide Two & Three- Set the background image for this slide in the line below -->
          <div onclick="document.location.href = 'index.php?uc=produits&co=newprod&prodid=<?php echo $nouveaute['IdProduit']?>'" class="carousel-item" style="background-image: url('images/nouveau_produits/pub<?php echo $nouveaute['IdProduit']?>.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <p></p>
            </div>
          </div>
		  <?php }?>
          
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>

    <!-- Page Content -->
    <div class="container">

      <h1 class="my-4">Bienvenue sur Cabouffe Web</h1>

      <!-- Marketing Icons Section 
      <div class="row">
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">Des Prix A Gogo !</h4>
            <div class="card-body">
				<a href="#quelqueprix"><img class="imgaccueil" src="images/reduction.jpg"></a>
            </div>
			<div class="card-footer">
				<p class="card-text">Si vous vous inscrivez de suite, vous aurez la possibilité d'avoir des réductions allant jusqu'à 70% sur certain produits</p>         
			</div>
          </div>
        </div>
		
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">Avec vos partenaires</h4>
            <div class="card-body">
				<img class="imgaccueil" src="images/entete_nos-partenaires.jpg">
            </div>
            <div class="card-footer">
				<p class="card-text">Nous vous garantissons une qualité et un savoir faire sur nos produits. Nos Patenaires choisi selon vos besoins.</p>            
			</div>
          </div>
        </div>
        
		<div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">Choix du paiement</h4>
            <div class="card-body">
           		<img class="imgaccueil" src="images/card.png">
            </div>
            <div class="card-footer">
				<p class="card-text">Vous pourez payer tout vos produit en toute garanti avec nos différents moyens de paiements sécurisés !<i class="fa fa-check fa-2x"></i></p>      
			</div>
          </div>
        </div>
      </div>
      <!-- /.row -->

	  <br/>
	  <br/>
	  <br/>
	  
      <!-- Portfolio Section -->
      <h2 id="quelqueprix"></h2>
	  <h2 >Quelques Produits !</h2>
	  <br/>
	  <div class="row" >
		<?php while ($produits = $resultprods->fetch()){
		?>
		<div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a  <?php 
						/*if ($_SESSION['modadmin'] != 'Client')
						{
							echo "data-toggle=\"modal\" data-target=\"#login-modal\"";
						}
						else
						{*/
							echo "data-toggle=\"modal\" data-target=\"#product_Descript\"";/*
						} */
						if ($produits['ProduitEnPromotion'] == TRUE)
						{
							$reduprix =($produits['PrixProduit'] - ($produits['PrixProduit'] * $produits['ReductionProduit'])/100);
						}
						else
						{
							$reduprix =0;
						} 
					?> onclick="liendescription(<?php echo $produits['IdProduit'].",'".addslashes($produits['NomProduit'])."','".addslashes($produits['Descriptif'])."',".$produits['PrixProduit'].",".$reduprix ?>)">
			<img class="card-img-top imageprod" src="images/produits/<?php echo $produits['IdProduit'] ?>.png" alt="<?php echo $produits['NomProduit'] ?>"/></a>
            <div class="card-body">
              <h4 class="card-title">
                <?php echo $produits['NomProduit'] ?>
              </h4>
			  <div class=" justify-content-center d-flex">
					<p class="card-text h1 text-danger"><?php echo $produits['PrixProduit'] - ($produits['PrixProduit']*$produits['ReductionProduit'])/100 ?> €</p> 
					<p class="card-text h4">(- <?php echo $produits['ReductionProduit'] ?> %)</p>
				</div>
				<button type="button" class="btn btn-success" 
					<?php 
						if ($_SESSION['modadmin'] != 'Client')
						{
							echo "data-toggle=\"modal\" data-target=\"#login-modal\"";
						}
						else
						{
							echo "data-toggle=\"modal\" data-target=\"#product_view\"";
						} 
						if ($produits['ProduitEnPromotion'] == TRUE)
						{
							$reduprix =($produits['PrixProduit'] - ($produits['PrixProduit'] * $produits['ReductionProduit'])/100);
						}
						else
						{
							$reduprix =0;
						} 
					?> onclick="lienpopupprod(<?php echo $produits['IdProduit'].",'".addslashes($produits['NomProduit'])."',".$produits['CategorieProduit'].",".$produits['StockProduit'].",".$produits['PrixProduit'].",".$reduprix ?>)" >
					
					Mettre dans le panier
					
				</button>
			</div>
          </div>
        </div>
		<?php
		}
		?>  
	</div>
      <!-- /.row -->
</div>
      










