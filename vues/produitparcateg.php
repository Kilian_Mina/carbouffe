<div class="container">
<!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3"><?php echo $_REQUEST['cat']; ?></h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Accueil</a>
        </li>
		<li class="breadcrumb-item">
          <a href="index.php?uc=produits">Nos Produits</a>
        </li>
        <li class="breadcrumb-item active"><?php echo $_REQUEST['cat']; ?></li>
      </ol>

	<br/><br/><br/>

	<div class="row">
		<?php
			while ($ProdParCateg = $AllProdParCateg->fetch()){
		?>

		<div class="col-lg-4 col-sm-6 text-center mb-6 margingbottom">
			<a  <?php 
						/*if ($_SESSION['modadmin'] != 'Client')
						{
							echo "data-toggle=\"modal\" data-target=\"#login-modal\"";
						}
						else
						{*/
							echo "data-toggle=\"modal\" data-target=\"#product_Descript\"";
						//} 
						if ($ProdParCateg['ProduitEnPromotion'] == TRUE)
						{
							$reduprix =($ProdParCateg['PrixProduit'] - ($ProdParCateg['PrixProduit'] * $ProdParCateg['ReductionProduit'])/100);
						}
						else
						{
							$reduprix =0;
						} 
					?> onclick="liendescription(<?php echo $ProdParCateg['IdProduit'].",'".addslashes($ProdParCateg['NomProduit'])."','".addslashes($ProdParCateg['Descriptif'])."',".$ProdParCateg['PrixProduit'].",".$reduprix ?>)"> <img class="rounded-circle img-fluid d-block mx-auto" src="images/produits/<?php echo $ProdParCateg['IdProduit']; ?>.png" alt="<?php echo $ProdParCateg['NomProduit']; ?>" style="background-color:rgba(240, 240, 240, 0.7); width: 150px;">
			<h5><?php echo $ProdParCateg['NomProduit']; ?></h5> </a>
			<p><h4><?php if($ProdParCateg['ProduitEnPromotion'] == TRUE) echo (($ProdParCateg['PrixProduit'] - ($ProdParCateg['PrixProduit'] * $ProdParCateg['ReductionProduit'])/100)); else echo $ProdParCateg['PrixProduit']; ?> €</h4></p>
			<button type="button" class="btn btn-success" 
					<?php 
						if ($_SESSION['modadmin'] != 'Client')
						{
							echo "data-toggle=\"modal\" data-target=\"#login-modal\"";
						}
						else
						{
							echo "data-toggle=\"modal\" data-target=\"#product_view\"";
						} 
						if ($ProdParCateg['ProduitEnPromotion'] == TRUE)
						{
							$reduprix =($ProdParCateg['PrixProduit'] - ($ProdParCateg['PrixProduit'] * $ProdParCateg['ReductionProduit'])/100);
						}
						else
						{
							$reduprix =0;
						} 
					?> onclick="lienpopupprod(<?php echo $ProdParCateg['IdProduit'].",'".addslashes($ProdParCateg['NomProduit'])."',".$ProdParCateg['CategorieProduit'].",".$ProdParCateg['StockProduit'].",".$ProdParCateg['PrixProduit'].",".$reduprix ?>)" >
					Mettre dans le panier
				</button>
		</div>
		
		
		<?php
			}
		?>	
	</div>
</div>