<div class="container text-center">
    <h1 class="my-4"><?php echo $produit['NomProduit']; ?></h1>
    <a  <?php
    if ($_SESSION['modadmin'] != 'Client') {
        echo "data-toggle=\"modal\" data-target=\"#login-modal\"";
    } else {
        echo "data-toggle=\"modal\" data-target=\"#product_Descript\"";
    }
    if ($produit['ProduitEnPromotion'] == true) {
        $reduprix = ($produit['PrixProduit'] - ($produit['PrixProduit'] * $produit['ReductionProduit']) / 100);
    } else {
        $reduprix = 0;
    }
    ?> onclick="liendescription(<?php echo $produit['IdProduit'] . ",'" . addslashes($produit['NomProduit']) . "','" . addslashes($produit['Descriptif']) . "'," . $produit['PrixProduit'] . "," . $reduprix ?>)">
        <img class="imgaccueil" src="images/produits/<?php echo $produit['IdProduit'] ?>.png" style="width:250px" /></a><br/><br/>
    <!-- Marketing Icons Section -->
    <div class="row justify-content-center">
        <div class="col-lg-4 mb-4">
            <div class="card h-100">
                <h4 class="card-header">Description</h4>
                <div class="card-body">
                    <?php echo $produit['Descriptif'] ?>
                </div>
                <div class="card-footer">
                    <h4 class="card-text">Son Prix ?</h4>		
                </div>
                <div class="card-body">
                    <h2><p class="card-text "><?php
                            if ($reduprix != 0) {
                                echo $reduprix;
                            } else {
                                echo $produit['PrixProduit'];
                            }
                            ?>€</p></h2>
                </div>



                <button type="button" class="btn btn-success" 
                        <?php
                        if ($_SESSION['modadmin'] != 'Client') {
                            echo "data-toggle=\"modal\" data-target=\"#login-modal\"";
                        } else {
                            echo "data-toggle=\"modal\" data-target=\"#product_view\"";
                        }
                        if ($produit['ProduitEnPromotion'] == true) {
                            $reduprix = ($produit['PrixProduit'] - (($produit['PrixProduit'] * $produit['ReductionProduit']) / 100));
                        } else {
                            $reduprix = 0;
                        }
                        ?> onclick="lienpopupprod(<?php echo $produit['IdProduit'] . ",'" . addslashes($produit['NomProduit']) . "'," . $produit['CategorieProduit'] . "," . $produit['StockProduit'] . "," . $produit['PrixProduit'] . "," . $reduprix ?>)" >

                    Mettre dans le panier

                </button>
            </div>
        </div>
    </div>
</div>