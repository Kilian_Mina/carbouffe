    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Offres 
        <small>Spéciales</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Accueil</a>
        </li>
        <li class="breadcrumb-item active">Offres Spéciales</li>
      </ol>

      <div class="row">

		<?php 
			while ($produits = $HuitOffresSpeciales->fetch()){
		?>
	  
		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
			<div class="card h-100">
				<a  <?php 
						/*if ($_SESSION['modadmin'] != 'Client')
						{
							echo "data-toggle=\"modal\" data-target=\"#login-modal\"";
						}
						else
						{*/
							echo "data-toggle=\"modal\" data-target=\"#product_Descript\"";
						//}
						if ($produits['ReductionProduit'] == TRUE)
						{
							$reduprix =($produits['PrixProduit'] - ($produits['PrixProduit'] * $produits['ReductionProduit'])/100);
						}
						else
						{
							$reduprix =0;
						} 
					?> onclick="liendescription(<?php echo $produits['IdProduit'].",'".addslashes($produits['NomProduit'])."','".addslashes($produits['Descriptif'])."',".$produits['PrixProduit'].",".$reduprix ?>)">
					<img class="card-img-top" src="images/produits/<?php echo $produits['IdProduit'] ?>.png" alt="<?php echo $produits['NomProduit'] ?>"/></a>
				<div class="card-body">
					<h4 class="card-title">
					<a href="#" <?php if ($_SESSION['modadmin'] != 'Client'){echo "data-toggle=\"modal\" data-target=\"#login-modal\"";} ?> ><?php echo $produits['NomProduit'] ?></a>
					</h4>
					<p class="card-text h1 text-danger"><?php echo $produits['PrixProduit'] - ($produits['PrixProduit']*$produits['ReductionProduit'])/100 ?> €</p> 
					<p class="card-text h4">(- <?php echo $produits['ReductionProduit'] ?> %)</p>
				</div>
				<button type="button" class="btn btn-success" 
					<?php 
						if ($_SESSION['modadmin'] != 'Client')
						{
							echo "data-toggle=\"modal\" data-target=\"#login-modal\"";
						}
						else
						{
							echo "data-toggle=\"modal\" data-target=\"#product_view\"";
						} 
						if ($produits['ReductionProduit'] == TRUE)
						{
							$reduprix =($produits['PrixProduit'] - ($produits['PrixProduit'] * $produits['ReductionProduit'])/100);
						}
						else
						{
							$reduprix =0;
						} 
                                                
					?> onclick="lienpopupprod(<?php echo $produits['IdProduit'].",'".addslashes($produits['NomProduit'])."',".$produits['CategorieProduit'].",".$produits['StockProduit'].",".$produits['PrixProduit'].",".$reduprix ?>)" >
					
					Mettre dans le panier
					
				</button>
			</div>
        </div>
		
		<?php
			}
		?> 
    </div>
</div>
    <!-- /.container -->
