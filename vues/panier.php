<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">Votre
        <small>Panier</small>
    </h1>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.php">Accueil</a>
        </li>
        <li class="breadcrumb-item active">Votre Panier</li>
    </ol>

    <br/><br/><br/><br/>

    <h2>Vos Produits</h2>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>N°</th>
            <th>Produit</th>
            <th>Quantité</th>
            <th>Nom</th>
            <th>Prix</th>
            <th>Supprimer</th>
        </tr>
        </thead>
        <tbody>
        <?php

        $i = 1;
        while ($Panier = $AllPanier1->fetch()) { ?>

            <tr>
                <td><?php echo $i;
                    $i++; ?></td>
                <td>
                    <img src="images/produits/<?php echo $Panier['IdProduit'] ?>.png" class="rounded-top"
                         alt="Sample image">
                </td>
                <td>
                    <div class="row">
                        <button class="button fa fa-minus" onclick="
                                if(<?php echo $Panier['Qteproduit'] ?> != 1)
                                {
                                document.location.replace('index.php?uc=panier&co=modifproduitcommande&idcmd=<?php echo $Panier['IdCommande'] ?>&Qte=<?php echo $Panier['Qteproduit'] - 1 ?>')
                                }
                                else{
                                if(confirm('Êtes vous sur de vouloir suprimer se produit ?'))
                                {
                                document.location.replace('index.php?uc=panier&co=supprprod&idcmd=<?php echo $Panier['IdCommande'] ?>');
                                }
                                }">
                        </button>

                        <button class="" disabled><span><?php echo $Panier['Qteproduit'] ?></span></button>

                        <button class="button fa fa-plus"
                                onclick="document.location.replace('index.php?uc=panier&co=modifproduitcommande&idcmd=<?php echo $Panier['IdCommande'] ?>&Qte=<?php echo $Panier['Qteproduit'] + 1 ?>')">
                            &nbsp;
                        </button>
                    </div>
                </td>
                <td><?php echo $Panier['NomProduit'] ?></td>
                <td><?php if ($Panier['ProduitEnPromotion'] == FALSE) {
                        echo $Panier['PrixProduit'] * $Panier['Qteproduit'];
                    } else {
                        echo ($Panier['PrixProduit'] - (($Panier['PrixProduit'] * $Panier['ReductionProduit']) / 100)) * $Panier['Qteproduit'];
                    } ?>€
                </td>
                <td>
                    <div class="rem">
                        <button class="fa fa-close"
                                onclick="if (confirm('Êtes vous sur de continuer ?')){document.location.replace('index.php?uc=panier&co=supprprod&idcmd=<?php echo $Panier['IdCommande'] ?>');}"></button>
                    </div>
                </td>
            </tr>

        <?php } ?>
        </tbody>
    </table>
    <br/><br/><br/><br/><br/>
    <div class="col-md-6 checkout-left-basket">
        <h2>Ticket</h2>

        <table class="table">
            <thead>
            <tr>
                <th>Qte</th>
                <th>Produit</th>
                <th>Unité</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $total = 0;
            while ($Panier = $AllPanier2->fetch()) {
                ?>
                <tr>
                    <td><?php echo $Panier['Qteproduit']; ?></td>
                    <td><?php echo $Panier['NomProduit']; ?></td>
                    <td><?php if ($Panier['ProduitEnPromotion'] == FALSE) {
                            echo $Panier['PrixProduit'];
                        } else {
                            echo ($Panier['PrixProduit']-(($Panier['PrixProduit']*$Panier['ReductionProduit'])/100) );
                        }; ?>€
                    </td>
                    <td><?php if ($Panier['ProduitEnPromotion'] == FALSE) {
                            echo $Panier['PrixProduit'] * $Panier['Qteproduit'];
                        } else {
                            echo ($Panier['PrixProduit']-(($Panier['PrixProduit']*$Panier['ReductionProduit'])/100) ) * $Panier['Qteproduit'];
                        } ?>€
                    </td>
                </tr>
                <?php
                if ($Panier['ProduitEnPromotion'] == FALSE) {
                    $total = $total + ($Panier['PrixProduit'] * $Panier['Qteproduit']);
                } else {

                    $total = $total + (($Panier['PrixProduit']-(($Panier['PrixProduit']*$Panier['ReductionProduit'])/100) ) * $Panier['Qteproduit']);
                }
            }
            ?>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>TVA<br/>(2.10%)</td>
                <td></td>
                <td></td>
                <td> <?php echo number_format((($total * 2.10) / 100), 2, ',', ' '); ?>€</td>
            </tr>
            <tr>
                <td>Total<br/>(TVA incl)</td>
                <td></td>
                <td></td>
                <td><?php echo number_format(($total + (($total * 2.10) / 100)), 2, ',', ' '); ?>€</td>
            </tr>
            </tbody>
        </table>
    </div>
    <button type="button" class="btn btn-success btn-lg btn-block"
            onclick="document.location.replace('index.php?uc=panier&co=payement')">Cliquez ici si vous avez fini !!
    </button>
    <br/><br/><br/><br/><br/>
</div>
<!-- /.container -->
