
function checkMdp() {
	
	document.getElementById("mdperror").style.display = "inline";
	var mdp = document.getElementById("pass").value;
	var mdp2 = document.getElementById("confirmpass").value;
	
	if (mdp!=mdp2) {
		document.getElementById("mdperror").innerHTML = "Mot de passe invalide";
		var register = document.getElementById("register").disabled = true;
	}
	else{
		document.getElementById("mdperror").style.display = "none";
		var register = document.getElementById("register").disabled = false;
	}
}