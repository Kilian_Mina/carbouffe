function liendescription(Idl, Noml, Descriptionl, Prixl, Reducl) {

    document.getElementById('NomProdl').innerHTML = Noml;
    document.getElementById('ImageIdProdl').innerHTML = "<img src=\"images/produits/" + Idl + ".png\" class=\"img-responsive\" style=\"width:200px\">";
    document.getElementById('Descriptifl').innerHTML = Descriptionl;

    if (Reducl == 0) {
        document.getElementById('PrixProdl').innerHTML = " ";
        document.getElementById('ReducProdl').innerHTML = Prixl + " €";
    } else {
        document.getElementById('PrixProdl').innerHTML = Prixl + " €";
        document.getElementById('ReducProdl').innerHTML = Reducl + " €";
    }

    document.getElementById('MaxStockl').max = Stockl;
}

function lienpopupprod(Id, Nom, Categ, Stock, Prix, Reduc) {
    document.getElementById('NomProd').innerHTML = Nom;
    document.getElementById('ImageIdProd').innerHTML = "<img src=\"images/produits/" + Id + ".png\" class=\"img-responsive\" style=\"width:200px\">";
    document.getElementById('IdProd').innerHTML = Id;
    //document.getElementById('StockProd').innerHTML=Stock;

    if (Reduc === 0) {
        document.getElementById('PrixProd').innerHTML = " ";
        document.getElementById('ReducProd').innerHTML = Prix + " €";
    } else {
        document.getElementById('PrixProd').innerHTML = Prix + " €";
        document.getElementById('ReducProd').innerHTML = Reduc + " €";
    }


    document.getElementById('MaxStock').max = Stock;
    document.getElementById('formIdProd').value = Id;
}

