--
-- Déchargement des données de la table `administrateur`
--

INSERT INTO `administrateur` (`IdAdmin`, `NomAdmin`,`PrenomAdmin`, `LogAdmin`, `PassAdmin`) VALUES
  (1, 'MINA', 'Kilian', 'kilian.minapro@gmail.com', '$2y$10$a1BYJCJeH2KdhaT2I44sae0UCyIOPDj5Z9sblMR90Gv863AQehk.q'),
(2,'Professeur','Examinator', 'prof.exam@gmail.com', '$2y$10$RERlM/opjzKXYWf5/3vNGOyLk5xl6oWT3M2uPUq6u0GRkxijccwaq');

-- --------------------------------------------------------

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`IdClient`, `NomClient`, `PrenomClient`, `AdresseClient`, `EmailClient`, `DateNaissClient`, `CodePostalClient`, `PassClient`) VALUES
(1, 'DUBOIS', 'Alexia', '10 Chemin Comètes', 'alexia.dub@gmail.com',NULL, 97417, '$2y$10$7KeLkoPM2bsXf.CMwEfrceZFYvQt/ujxz.PPElk8HBVd6k5KVxTHm'),
(2, 'CHEVALIER', 'Arthus', '10 Allée des Manguiers', 'arthuschevalier1@outlook.fr',NULL, 97400, '$2y$10$./ulUNreIxgIrQWnnRW58Okeocg7Iz08xiCmb1rMRoogOVsohdmra'),
(3, 'LEROY', 'Catherine', '14 Chemin Finette', 'catherine.leroy@yahoo.fr',NULL, 97440, '$2y$10$VveZOhomHzt2f0mZEqR6hOv2.SpCrSlcOyrnQliQWqrHRgKN4cMZ6'),
(4, 'BARBIER', 'César', '68 Allée de la Découverte', 'barbier.cesar2@gmail.com',NULL, 97400, '$2y$10$T4Ay4fLqthHHyVlvWzIpxuHgXQVIzYHNeQAMn3ZoTJwkLJzMxdzmK'),
(5, 'DUVAL', 'Cassandre', '22 Rue de la Vérité', 'cassandre.duval@outlook.com',NULL, 97417, '$2y$10$BBnozMSQRTpbNyNeEkJkyu2AC7pVAnPKIYvBKNm88L7a0.eG0IARG'),
(6, 'BOURGEOIS', 'Geoffrey', 'Appt 18, Rue Rolland Garros', 'geoffrey.bourg974@gmail.com',NULL, 97400, '$2y$10$WSFBQ0OjyeF7/lXGYmamGOF0o23.50G8OsssvcW6i0nYrBQqM6oZ.'),
(7, 'DESCHAMPS', 'Danaé', '66 Chemin du Paradis', 'danae.deschamps@outlook.fr',NULL, 97440, '$2y$10$yp7TzzwrWK1vX3eEV6k3X.Lxob8RmxoRK9uV4GqziMOeSVwp3GnRy'),
(8, 'BAILLY', 'Lola', '68 Chemin Comètes', 'lola.bailly4@yahoo.com',NULL, 97417, '$2y$10$.zrEyhOesvXwRk.OBF6mSOxlds6DHvt/rik5S7.6g0wMcRy490FRW'),
(9, 'LUKE', 'Lucky', '77 Allée de la Découverte', 'luckyluke417@gmail.com',NULL, 97417, '$2y$10$WOnzJtfiY2JITwCQCCbUPe5YtOdd2XQATqNMNYD21/ERuX1F6aihS'),
(10, 'DANIEL', 'Ethan', 'Appt 8, Rue Rolland Garros', 'daniel.ethan10@outlook.com',NULL, 97440, '$2y$10$z68DPdmVtprnI1PD3PI/VOhWaHgIoT0kOC8706ERymanql/Ekpyqe'),
  (11, 'MINA', 'Kilian', 'Bat 1 Appt 10 Résidence des facs Saint-Denis', 'kilian.minapro@gmail.com', '1998-03-26', 97400, '$2y$10$Cn/QNkW5lZh/wUvvh.lpR.IqrGxlezJybqxQh1LYe2vBgzL7YxQPu'),
(12, 'Professeur', 'Examinator', 'Lyc�e Bellepierre', 'prof.exam@gmail.com', '1960-05-20', 97436, '$2y$10$8IfSF5tXMnYxfzahDPE/EuyFDgjHNfHPmlRit2oPjouG4nJD4caZi');

-- --------------------------------------------------------

--
-- Déchargement des données de la table `categorie_produits`
--

INSERT INTO `categorieproduits` (`IdCategorie`, `NomCategorie`, `DescriptionCategorie`) VALUES
(1, 'Animaux', 'Avec cela, vos animaux ne manquerons de rien ! Quelque soit les préférence de votre animal, il sera le plus heureux !'),
(2, 'Boisson Gazeuse', 'Vous êtes inviter a une fête mais vous ne savez pas quoi ramener ? Nous avons la solution ! ici vous trouverez toute les boisson gazeuse que vous souhaitez'),
(3, 'Boisson Naturel', 'Une petite soif ? Un matin dur ? Ne stresser plus ! vous avez un grand panel de choix de jus de fruit !'),
(4, 'Cereale', 'Un large choix de pain s\'offre a vous ! n\'hésitez pas a y faire un tour'),
(5, 'Autre', 'la poubelle'),
(6, 'Pates', 'Une petite faim, les nouilles instantanées sont faites pour vous ! venez y faire un tour'),
(7, 'Ustensiles', 'Tuperoire, casserole, poelle ? vos ustensiles sont classer dans cette rubrique '),
(8, 'Entretien', 'Besoin de produit vaisselle, de mouchoirs ? Cette rubrique vous donne un vaste panel de produits les concernant !'),
(9, 'Surgele', 'En famille ou seul, avec leur conservation à long termes, vous avez de quoi faire dans notre rayons surgelé'),
(10, 'Conserve', 'vous voulez faire des réserves cette rubrique va vous intéresser ! Venez y jeter un coup d\'oeil '),
(11, 'Sport', 'Vous voulez entretenir votre corps ? ici vous trouverez se que vous avez besoin !!'),
(12, 'Fruits', 'Rien de tel que des Fruit frais pour votre salade de fruit ! seriez vous relevez le défit ?'),
(13, 'Legumes', 'Les meilleurs petits plats sont ceux fait avec amour ... mais aussi avec de superbe légume tout droit sortie d\'un champ ! ');

-- --------------------------------------------------------


--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`IdProduit`, `NomProduit`, `CategorieProduit`, `StockProduit`, `PrixProduit`, `ProduitEnPromotion`, `ReductionProduit`,`NouveauProduit`, `DteAfficheDebutNouveauProduit`, `DteAfficheFinNouveauProduit`, `Descriptif`) VALUES
(1, 'Huile de tournesol FORTUNE', 5, 50, 6, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(2, 'Pepsi (2L)', 2, 50, 5, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(3, 'Riz Basmati (5Kg)', 5, 50, 10, TRUE, 30, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(4, 'Nourriture pour chiots ROYAL C', 1, 50, 8, TRUE, 20, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(5, 'Soupe instantanée KNORR (100gr', 6, 50, 3, TRUE, 10, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(6, 'Nouilles chinoises CHINGS (75g', 6, 50, 4, TRUE, 20, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(7, 'Biscuits apéritifs LASHUN SEV ', 4, 50, 3, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(8, 'Pain Brioché (300gr)', 4, 50, 4, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(9, 'Epinards', 13, 50, 2, TRUE, 30, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(10, 'Mangues (1Kg)', 12, 50, 4, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(11, 'Pommes Rouges (1Kg)', 12, 50, 5, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(12, 'Brocolis (500gr)', 13, 50, 3, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(13, 'Jus de Fruit TROPICANA (1L)', 3, 50, 3, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(14, 'Jus de Prunes SUNSWEET (1L)', 3, 50, 3, TRUE, 20, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(15, 'Coca Cola Zero (330mL)', 2, 50, 3, TRUE, 50, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(16, 'Sprite (2L)', 2, 50, 3, TRUE, 50, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(17, 'Gel Nettoyant VIM (1.5L)', 8, 50, 7, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(18, 'Dosettes lave-vaiselle VIM (50', 8, 50, 2, TRUE, 50, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(19, 'Mouchoirs ODONIL (50gr)', 8, 50, 3, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(20, 'Nettoyant toilettes DOMEX (1L)', 8, 50, 5, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(21, 'Ensemble de contenants PRINCEW', 7, 50, 7, TRUE, 30, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(22, 'Conteneur SIGNORAWARE (900mL)', 7, 50, 4, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(23, 'Casserole simple', 7, 50, 5, TRUE, 40, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(24, 'Omega Acier inoxydable', 7, 50, 5, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(25, 'Nourriture pour chien adultes ', 1, 50, 3, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(26, 'Nourriture pour chien jeunes a', 1, 50, 5, TRUE, 50, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(27, 'Nourriture pour chat WHISKAS (', 1, 50, 5, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(28, 'Pâté pour chien PEDIGREE (400g', 1, 50, 6, TRUE, 40, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(29, 'Bananes (1Kg)', 12, 50, 7, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(30, 'Choux fleur (2)', 13, 50, 4, TRUE, 50, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(31, 'Aubergines (1Kg)', 13, 50, 2, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(32, 'Citron Vert (500gr)', 12, 50, 5, TRUE, 10, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(33, 'Oignons (1Kg)', 13, 50, 5, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(34, 'Melon (1Kg)', 12, 50, 4, TRUE, 20, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(35, 'Champignons (500mg)', 13, 50, 9, TRUE, 10, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(36, 'Fraises (1)', 12, 50, 7, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(37, 'Pains aux raisins (2 en 1)', 4, 50, 4, TRUE, 20, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(38, 'Croissants au beurre (50gr)', 4, 50, 2, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(39, 'Pain blanc pita (250gr)', 4, 50, 3, TRUE, 50, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(40, 'Pain spécial Hots Dogs (150gr)', 4, 50, 4, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(41, 'Pain complet blanc (500gr)', 4, 50, 3, TRUE, 40, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(42, 'Rouleaux au chocolat (3 pcs)', 4, 50, 5, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(43, 'Pavés de masala (500gr)', 4, 50, 6, TRUE, 30, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(44, 'Pain blanc (200gr)', 4, 50, 6, TRUE, 30, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(45, 'Brioche (250gr)', 4, 50, 6, TRUE, 40, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(46, 'Assortiment de muffins (200gr)', 4, 50, 4, TRUE, 20, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(47, 'Bagels au sésame (200gr)', 4, 50, 6, TRUE, 10, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(48, 'Pain de lin aux noix (400gr)', 4, 50, 7, TRUE, 30, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(49, 'Soda à l orange (250mL)', 2, 50, 5, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(50, 'Jus d aamras (250mL)', 3, 50, 4, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(51, 'Eau de coco (1000mL)', 3, 50, 6, TRUE, 40, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(52, 'Jus d orange CERES (1L)', 3, 50, 6, TRUE, 30, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(53, 'Glucose D DABUR (250gr)', 11, 50, 9, TRUE, 20, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(54, 'Fortifiant au citron (50gr)', 11, 50, 8, TRUE, 40, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(55, 'Schweppes tonic (250mL)', 2, 50, 6, TRUE, 50, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(56, 'Red Bull (250mL)', 2, 50, 7, TRUE, 10, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(57, 'Pâté pour chats WHISKAS (400gr', 1, 50, 8, TRUE, 30, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(58, 'Nourriture pour chatons WHISKA', 1, 50, 5, TRUE, 30, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(59, 'Croquettes pour chiens PEDIGRE', 1, 50, 15, TRUE, 30, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(60, 'Bâtonnets de viande pour chien', 1, 50, 8, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(61, 'Pack dentaire pour les chiens ', 1, 50, 5, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(62, 'Bâtonnets à croquer pour chien', 1, 50, 6, TRUE, 40, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(63, 'Nourriture pour chats (90gr)', 1, 50, 6, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(64, 'Salami au poivre (250gr)', 9, 50, 12, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(65, 'Petits pois SUMERU (500gr)', 9, 50, 9, TRUE, 30, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(66, 'Poulet Tikka YUMMIEZ (300gr)', 9, 50, 6, TRUE, 20, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(67, 'Mélange de légumes (500gr)', 9, 50, 6, TRUE, 30, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(68, 'Pulpe de mangue (800gr)', 10, 50, 9, TRUE, 30, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(69, 'Pulpe de mangue kesar (800gr)', 10, 50, 5, TRUE, 50, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(70, 'Maïs (250gr)', 9, 50, 6, TRUE, 30, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(71, 'Nuggets au poulet (1Kg)', 9, 50, 4, TRUE, 40, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(72, 'Bâtonnets de poisson SUMMIEZ (', 9, 50, 5, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(73, 'Bâtonnets de poisson (200gr)', 9, 50, 8, FALSE, NULL, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(74, 'Bâtonnets de poisson sumeru (5', 9, 50, 11, TRUE, 40, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(75, 'Nems aux légumes (400gr)', 9, 50, 7, TRUE, 40, FALSE, NULL, NULL, 'Voici la description de se produit ...'),
(78, 'Brownie choco + beurre salé', 4, 15, 8, TRUE, 10, TRUE, '2017-10-18', '2018-12-21', 'Voici la description de se produit ...'),
(79, 'Fromage Président au chèvre', 5, 80, 10, TRUE, 20, TRUE, '2017-10-20', '2018-12-14', 'Voici la description de se produit ...'),
(77, 'Glace M&M\'s', 9, 30, 5, FALSE, NULL, TRUE, '2017-10-19', '2018-11-19', 'Voici la description de se produit ...');
