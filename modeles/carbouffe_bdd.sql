-- version 2.0
-- Généré le :  dim. 15 avr. 2018 à 10:28


--
-- Base de données :  `Carbouffe_bdd`
--
CREATE DATABASE IF NOT EXISTS `carbouffe_bdd`;
USE `carbouffe_bdd`;

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

DROP TABLE IF EXISTS `administrateur`;
CREATE TABLE IF NOT EXISTS `administrateur` (
  `IdAdmin` int(11) NOT NULL AUTO_INCREMENT,
  `NomAdmin` varchar(100) DEFAULT NULL,
  `PrenomAdmin` varchar(100) DEFAULT NULL,
  `LogAdmin` varchar(100) NOT NULL,
  `PassAdmin` varchar(100) NOT NULL,
  PRIMARY KEY (`IdAdmin`)
);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `IdClient` int(11) NOT NULL AUTO_INCREMENT,
  `NomClient` varchar(100) NOT NULL,
  `PrenomClient` varchar(100) NOT NULL,
  `AdresseClient` varchar(100) DEFAULT NULL,
  `EmailClient` varchar(100) NOT NULL,
  `DateNaissClient` date DEFAULT NULL,
  `CodePostalClient` int(5) DEFAULT NULL,
  `PassClient` varchar(100) NOT NULL,
  PRIMARY KEY (`IdClient`)
) ;

-- --------------------------------------------------------

--
-- Structure de la table `categorie_produits`
--

DROP TABLE IF EXISTS `categorieproduits`;
CREATE TABLE IF NOT EXISTS `categorieproduits` (
  `IdCategorie` int(11) NOT NULL AUTO_INCREMENT,
  `NomCategorie` varchar(100) NOT NULL,
  `DescriptionCategorie` text NOT NULL,
  PRIMARY KEY (`IdCategorie`)
) ;

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE IF NOT EXISTS `produits` (
  `IdProduit` int(11) NOT NULL AUTO_INCREMENT,
  `NomProduit` varchar(500) DEFAULT NULL,
  `CategorieProduit` int(11) DEFAULT NULL,
  `StockProduit` int(11) DEFAULT NULL,
  `PrixProduit` int(11) DEFAULT NULL,
  `ProduitEnPromotion` BOOLEAN DEFAULT FALSE,
  `ReductionProduit` int(11) DEFAULT NULL,
  `NouveauProduit` BOOLEAN DEFAULT FALSE,
  `DteAfficheDebutNouveauProduit` date DEFAULT NULL,
  `DteAfficheFinNouveauProduit` date DEFAULT NULL,
  `Descriptif` text NOT NULL,
  PRIMARY KEY (`IdProduit`),
  KEY `CategorieProduit` (`CategorieProduit`) USING BTREE
) ;

-- --------------------------------------------------------

--
-- Structure de la table `commandeproduit`
--

DROP TABLE IF EXISTS `commandeproduit`;
CREATE TABLE IF NOT EXISTS `commandeproduit` (
  `IdCommande` int(11) NOT NULL AUTO_INCREMENT,
  `IdClient` int(11) NOT NULL,
  `IdProduit` int(11) NOT NULL,
  `DateCommande` datetime NOT NULL,
  `Qteproduit` int(11) NOT NULL,
  PRIMARY KEY (`IdCommande`),
  KEY `IdClient` (`IdClient`) USING BTREE,
  KEY `IdProduit` (`IdProduit`) USING BTREE,
  KEY `DateCommande` (`DateCommande`) USING BTREE
) ;

-- --------------------------------------------------------

--
-- Contraintes pour la table `cmd_prod_clt`
--
ALTER TABLE `commandeproduit`
  ADD CONSTRAINT `CommandeProduitClient1` FOREIGN KEY (`IdClient`) REFERENCES `client` (`IdClient`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `CommandeProduitClient2` FOREIGN KEY (`IdProduit`) REFERENCES `produits` (`IdProduit`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `produits1` FOREIGN KEY (`CategorieProduit`) REFERENCES `categorieproduits` (`IdCategorie`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;
COMMIT;

