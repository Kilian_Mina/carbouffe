<?php

class cb
{

    private static $server = 'mysql:host=localhost';    /* <!-- Server - */
    private static $db = 'dbname=carbouffe_bdd';         /* <!-- Database - */
    private static $user = 'exuser';                      /* <!-- Username - */
    private static $pwd = '12-Soleil&';                           /* <!-- Password - */
    private static $myPdo;
    private static $myPdoHack = null;

    /* <!-- Create a new connection - */

    private function __construct()
    {
        cb::$myPdo = new PDO(cb::$server . ';' . cb::$db, cb::$user, cb::$pwd, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        /* <!-- Put UTF-8 encoding --> */
        cb::$myPdo->query("SET character SET utf8;");
    }

    /* <!-- Instantiate a new connection - */

    public static function getPdocb()
    {
        if (cb::$myPdoHack == null) {
            cb::$myPdoHack = new cb;
        }

        return cb::$myPdoHack;
    }

    //Connexion 
    public function getLogin($Email, $Mdp)
    {
        $requeteClient = cb::$myPdo->prepare("SELECT * FROM client WHERE  EmailClient = :email;");
        $requeteClient->bindParam(':email', $Email);
        $requeteClient->execute();
        $dataClient = $requeteClient->fetch();

        $requeteAdmin = cb::$myPdo->prepare("SELECT * FROM administrateur WHERE  LogAdmin = :email;");
        $requeteAdmin->bindParam(':email', $Email);
        $requeteAdmin->execute();
        $dataAdmin = $requeteAdmin->fetch();

        if (password_verify($Mdp, $dataClient['PassClient'])) {
            $_SESSION['modadmin'] = 'Client';
            $_SESSION['Client'] = $dataClient['IdClient'];
            echo "<script type='text/javascript'>document.location.replace('../index.php');</script>";
        } else if (password_verify($Mdp, $dataAdmin['PassAdmin'])) {
            $_SESSION['modadmin'] = 'Admin';
            $_SESSION['Client'] = $dataAdmin['IdAdmin'];
            echo "<script type='text/javascript'>document.location.replace('../admin/index.php?uc=accueil');</script>";
        } else {
            $_SESSION['modadmin'] = 'Visitor';
            echo "<script type='text/javascri     pt'>document.location.replace('../index.php?uc=connect&co=login');</script>";
        }
    }

    //enregistrement d'un nouveau CLIENT et connexion direct
    public function signinandco($nom, $prenom, $mail, $mdp, $dtenaiss, $addr, $cdepost)
    {

        $requeteRegister = cb::$myPdo->prepare("INSERT INTO `client` (`NomClient`, `PrenomClient`, `AdresseClient`, `EmailClient`, `DateNaiss_Client`, `CodePostalClient`, `PassClient`) VALUES (:nom,:prenom, :addr,:mail,:dtenaiss,:cdepost,:mdp)");
        $requeteRegister->bindParam(':nom', $nom);
        $requeteRegister->bindParam(':prenom', $prenom);
        $requeteRegister->bindParam(':mail', $mail);
        $requeteRegister->bindParam(':dtenaiss', $dtenaiss);
        $requeteRegister->bindParam(':addr', $addr);
        $requeteRegister->bindParam(':cdepost', $cdepost);
        $requeteRegister->bindParam(':mdp', $mdp);
        $requeteRegister->execute();


        $requeteLog = cb::$myPdo->prepare("SELECT IdClient FROM client WHERE  EmailClient = :email AND mdp_Client = :mdp;");
        $requeteLog->bindParam(':email', $mail);
        $requeteLog->bindParam(':mdp', $mdp);
        $requeteLog->execute();
        $dataregister = $requeteLog->fetch();

        $_SESSION['modadmin'] = 'Client';
        $_SESSION['Client'] = $dataregister['Id_Client'];
        echo "<script type='text/javascript'> document.location.replace('../index.php'); </script>";
    }

    //------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------//
    //Fonction d'affichage//
    //------------------------------------------------------------------------------------------//
    //TABLE CLIENT//

    public function affichetoutclients()
    {
        return cb::$myPdo->query("SELECT * FROM client");
    }

    public function afficheclientparID($idClient)
    {
        $requeteClientParID = cb::$myPdo->prepare("SELECT * FROM client WHERE  IdClient = :idClient;");
        $requeteClientParID->bindParam(':idClient', $idClient);
        $requeteClientParID->execute();
        return $requeteClientParID->fetch();
    }

    public function adrrclient($idClient)
    {
        $requeteadresseclient = cb::$myPdo->prepare("SELECT `AdresseClient`,`CodePostalClient`  FROM client WHERE  IdClient = :idClient;");
        $requeteadresseclient->bindParam(':idClient', $idClient);
        $requeteadresseclient->execute();
        return $requeteadresseclient->fetch();
    }

    //------------------------------------------------------------------------------------------//
    //TABLE COMMANDE-CLIENT

    public function AfficheProdPanier()
    {
        $requeteAfficheProduitPanier = cb::$myPdo->prepare("Select * from commandeproduit inner join produits on produits.IdProduit = commandeproduit.IdProduit inner join client on client.IdClient = commandeproduit.IdClient Where commandeproduit.IdClient = :idClient");
        $requeteAfficheProduitPanier->bindParam(':idClient', $_SESSION['Client']);
        $requeteAfficheProduitPanier->execute();
        return $requeteAfficheProduitPanier;
    }

    //------------------------------------------------------------------------------------------//
    //TABLE PRODUIT

    public function afficheProduit()
    {
        return cb::$myPdo->query("SELECT * FROM produits");
    }

    public function afficheRandomProd($NbLimit)
    {
        $requeteAfficheProdAlea = cb::$myPdo->prepare("SELECT * FROM produits where ProduitEnPromotion = TRUE ORDER BY RAND() LIMIT :NbLimit;");
        $requeteAfficheProdAlea->bindValue(':NbLimit', $NbLimit, PDO::PARAM_INT);
        $requeteAfficheProdAlea->execute();
        return $requeteAfficheProdAlea;
    }

    public function afficheProduitId($idprod)
    {
        $requeteAfficheProduitParID = cb::$myPdo->prepare("SELECT * FROM produits where IdProduit=:idprod;");
        $requeteAfficheProduitParID->bindValue(':idprod', $idprod);
        $requeteAfficheProduitParID->execute();
        return $requeteAfficheProduitParID->fetch();
    }

    public function IdDuNouveauProduit($nom, $categorie, $stock, $prix)
    {
        $requeteNouveauProduit = cb::$myPdo->prepare("SELECT IdProduit FROM produits where (`NomProduit`= :nom AND `CategorieProduit`= :categorie AND `StockProduit`= :stock AND `PrixProduit`= :prix);");
        $requeteNouveauProduit->bindValue(':nom', $nom);
        $requeteNouveauProduit->bindValue(':categorie', $categorie);
        $requeteNouveauProduit->bindValue(':stock', $stock);
        $requeteNouveauProduit->bindValue(':prix', $prix);
        $requeteNouveauProduit->execute();
        return $requeteNouveauProduit->fetch();
    }

    public function afficheProduitNom($nom)
    {
        $nomprod = htmlspecialchars_decode($nom);
        $requeteAfficheProduitParNom = cb::$myPdo->prepare("SELECT * FROM produits where NomProduit=:nom;");
        $requeteAfficheProduitParNom->bindValue(':nom', $nomprod);
        $requeteAfficheProduitParNom->execute();
        return $requeteAfficheProduitParNom->fetch();
    }

    //------------------------------------------------------------------------------------------//
    //TABLE PRODUIT ==> NOUVEAU-PRODUIT

    public function affichenouveaute()
    {
        return cb::$myPdo->query("Select * from produits WHERE ( (NouveauProduit = TRUE ) AND (DteAfficheFinNouveauProduit >= NOW() AND DteAfficheDebutNouveauProduit <= NOW()) ) ORDER BY DteAfficheDebutNouveauProduit ASC LIMIT 3");
    }

    public function afficheTouteNouveauteProduit()
    {
        return cb::$myPdo->query("Select * from produits WHERE NouveauProduit = TRUE; ");
    }

    public function afficheNewProduitId($idprod)
    {
        $requeteAficherNouveauProduitParID = cb::$myPdo->prepare("SELECT * FROM produits where IdProduit=:idprod AND NouveauProduit = TRUE;");
        $requeteAficherNouveauProduitParID->bindValue(':idprod', $idprod);
        $requeteAficherNouveauProduitParID->execute();
        return $requeteAficherNouveauProduitParID->fetch();
    }

    public function IdDuNouveauNouveauProduit($nom, $datedebut, $datefin, $idproduit)
    {
        $requeteAfficheIdDuNouveauNouveauProduit = cb::$myPdo->prepare("SELECT IdProduit FROM produits where ( NouveauProduit = TRUE AND (NomProduit=:nom and DteAfficheFinNouveauProduit = :datefin AND DteAfficheDebutNouveauProduit = :datedebut AND IdProd = :idproduit) ;");
        $requeteAfficheIdDuNouveauNouveauProduit->bindValue(':nom', $nom);
        $requeteAfficheIdDuNouveauNouveauProduit->bindValue(':datedebut', $datedebut);
        $requeteAfficheIdDuNouveauNouveauProduit->bindValue(':datefin', $datefin);
        $requeteAfficheIdDuNouveauNouveauProduit->bindValue(':idproduit', $idproduit);
        $requeteAfficheIdDuNouveauNouveauProduit->execute();
        return $requeteAfficheIdDuNouveauNouveauProduit->fetch();
    }

    //------------------------------------------------------------------------------------------//
    //TABLE CATEGORIE-PRODUIT

    public function afficheTouteCategProd()
    {
        return cb::$myPdo->query("Select * from categorieproduits");
    }

    public function afficheCategProd()
    {
        return cb::$myPdo->query("Select * from categorieproduits group by NomCategorie");
    }

    public function afficheCategorieProduitId($idprod)
    {
        $requeteAfficheCategoriePorduitParID = cb::$myPdo->prepare("SELECT * FROM categorieproduits where IdCategorie=:idprod;");
        $requeteAfficheCategoriePorduitParID->bindValue(':idprod', $idprod);
        $requeteAfficheCategoriePorduitParID->execute();
        return $requeteAfficheCategoriePorduitParID->fetch();
    }

    public function IdDeNouvelleCategorieProduit($nom, $Description)
    {
        $requeteAfficheIDNouvelleCategorieProduit = cb::$myPdo->prepare("SELECT IdCategorie FROM categorieproduits where (NomCategorie=:nom and DescriptionCategorie =:Description);");
        $requeteAfficheIDNouvelleCategorieProduit->bindValue(':nom', $nom);
        $requeteAfficheIDNouvelleCategorieProduit->bindValue(':Description', $Description);
        $requeteAfficheIDNouvelleCategorieProduit->execute();
        return $requeteAfficheIDNouvelleCategorieProduit->fetch();
    }

    //------------------------------------------------------------------------------------------//
    //COMBINAISON DE TABLES

    public function afficheProduitparCateg($Nprod)
    {
        $requeteAfficheProduitParCategorie = cb::$myPdo->prepare("SELECT * FROM produits inner join categorieproduits on categorieproduits.IdCategorie = produits.CategorieProduit WHERE  NomCategorie = :Nprod;");
        $requeteAfficheProduitParCategorie->bindParam(':Nprod', $Nprod);
        $requeteAfficheProduitParCategorie->execute();
        return $requeteAfficheProduitParCategorie;
    }

    public function afficheCategorieParIdProduit($idprod)
    {
        $requeteAfficheCategorieProduitParIDCategorieProduit = cb::$myPdo->prepare("SELECT * FROM produits inner join categorieproduits on categorieproduits.IdCategorie = produits.CategorieProduit WHERE  categorieproduits.IdCategorie= :idprod;");
        $requeteAfficheCategorieProduitParIDCategorieProduit->bindParam(':idprod', $idprod);
        $requeteAfficheCategorieProduitParIDCategorieProduit->execute();
        return $requeteAfficheCategorieProduitParIDCategorieProduit->fetch();
    }

    public function afficheCategDuProduitParId($idprod)
    {
        $requeteAfficheCategorieProduitParIDProduit = cb::$myPdo->prepare("SELECT * FROM produits inner join categorieproduits on categorieproduits.IdCategorie = produits.CategorieProduit WHERE  produits.IdProduit= :idprod;");
        $requeteAfficheCategorieProduitParIDProduit->bindParam(':idprod', $idprod);
        $requeteAfficheCategorieProduitParIDProduit->execute();
        return $requeteAfficheCategorieProduitParIDProduit->fetch();
    }

    //------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------//
    //Fonction d'ajout

    public function AjoutProdPanier($idClient, $idProduit, $Qte_cmd)
    {
        $dateaujourdhui = date('Y-m-d H:i:s');
        $requeteAjoutPrduitPanier = cb::$myPdo->prepare("INSERT INTO `commandeproduit`(`IdClient`, `IdProduit`, `DateCommande`, `Qteproduit`) VALUES (:idClient, :idProduit, :Dte_cmd, :Qte_cmd)");
        $requeteAjoutPrduitPanier->bindParam(':idClient', $idClient);
        $requeteAjoutPrduitPanier->bindParam(':idProduit', $idProduit);
        $requeteAjoutPrduitPanier->bindParam(':Dte_cmd', $dateaujourdhui);
        $requeteAjoutPrduitPanier->bindParam(':Qte_cmd', $Qte_cmd);
        $requeteAjoutPrduitPanier->execute();
    }

    //A refaire--------------------------------------------------------------------------------------------------------//
    public function ajoutproduit($nom, $categorie, $stock, $prix, $reduction, $description)
    {
        if ($reduction == '') {
            $enPromotion = FALSE;
            $reduction = NULL;
        };
        $requeteAjoutProduit = cb::$myPdo->prepare("INSERT INTO `produits`(`NomProduit`, `CategorieProduit`, `StockProduit`, `PrixProduit`, `ProduitEnPromotion`, `ReducProduit`, `Descriptif`) VALUES (:nom,:categorie,:stock,:prix,:enpromo,:reduction,:description)");
        $requeteAjoutProduit->bindParam(':nom', $nom);
        $requeteAjoutProduit->bindParam(':categorie', $categorie);
        $requeteAjoutProduit->bindParam(':stock', $stock);
        $requeteAjoutProduit->bindParam(':prix', $prix);
        $requeteAjoutProduit->bindParam(':enpromo', $enPromotion);
        $requeteAjoutProduit->bindParam(':reduction', $reduction);
        $requeteAjoutProduit->bindParam(':description', $description);
        $requeteAjoutProduit->execute();

        //Ajout d'un produit dans l'auto completion (AJAX)
        $requeteAfficheNomProduit = cb::$myPdo-- > query("SELECT `NomProduit` FROM `produits`");
        while ($donneeNomProduit = $requeteAfficheNomProduit->fetch()) {
            $text[] = $donneeNomProduit['NomProduit'];
        }
        $monfichier = fopen('../bin/produit.txt', 'r+');
        fputs($monfichier, serialize($text));
    }


//A Modifier
    public function ajoutnouveauproduit($nom, $datedebut, $datefin, $idproduit)
    {
        $requeteAjoutNouveauProduit = cb::$myPdo->prepare("INSERT INTO `nouveaute_produits`(`Nom_New_Prod`, `dte_debut_new_prod`, `dte_fin_new_prod`, `id_prod`) VALUES (:nom,:datedebut,:datefin,:idproduit)");
        $requeteAjoutNouveauProduit->bindParam(':nom', $nom);
        $requeteAjoutNouveauProduit->bindParam(':datedebut', $datedebut);
        $requeteAjoutNouveauProduit->bindParam(':datefin', $datefin);
        $requeteAjoutNouveauProduit->bindParam(':idproduit', $idproduit);
        $requeteAjoutNouveauProduit->execute();
    }

    //------------------------------------------------------------------------------------------------------------------//


    public function ajoutcategorieproduit($nom, $description)
    {
        $requeteAjoutCategorie = cb::$myPdo->prepare("INSERT INTO `categorieproduits`(`NomCategorie`, `DescriptionCategorie`) VALUES (:nom,:description)");
        $requeteAjoutCategorie->bindParam(':nom', $nom);
        $requeteAjoutCategorie->bindParam(':description', $description);
        $requeteAjoutCategorie->execute();
    }

    //------------------------------------------------------------------------------------------//
    //Fonction de suppression

    public function EffaceProduitPanier($idcmd)
    {
        $requeteEffacerProduitPanier = cb::$myPdo->prepare("DELETE FROM `commandeproduit` WHERE IdCommande = :idcmd");
        $requeteEffacerProduitPanier->bindParam(':idcmd', $idcmd);
        $requeteEffacerProduitPanier->execute();
    }

    public function Supprprod($idprod)
    {
        $requeteEffacerProduit = cb::$myPdo->prepare("DELETE FROM `produits` WHERE IdProduit = :idprod");
        $requeteEffacerProduit->bindParam(':idprod', $idprod);
        $requeteEffacerProduit->execute();
    }

    public function supprcategprod($idprod)
    {
        $requete = cb::$myPdo->prepare("DELETE FROM `categorieproduits` WHERE IdCategorie = :idprod");
        $requete->bindParam(':idprod', $idprod);
        $requete->execute();
    }

    //------------------------------------------------------------------------------------------//
    //Fonction de modification 

    public function modifProduitPanier($idcmd, $Qte)
    {
        $requeteModificationProduitPanier = cb::$myPdo->prepare("UPDATE `commandeproduit` SET `Qteproduit`=:Qte WHERE `IdCommande`=:idcmd");
        $requeteModificationProduitPanier->bindParam(':idcmd', $idcmd);
        $requeteModificationProduitPanier->bindParam(':Qte', $Qte);
        $requeteModificationProduitPanier->execute();
        echo "<script type='text/javascript'> document.location.replace('../index.php?uc=panier'); </script>";
    }

    public function modifproduit($id, $nom, $categorie, $stock, $prix, $reduction, $description)
    {

        if ($reduction == '') {
            $reduction = NULL;
            $enPromotion = FALSE;
        }

        $requeteModificationProduit = cb::$myPdo->prepare("UPDATE `produits` SET `NomProduit`=:nom,`CategorieProduit`=:categorie,`StockProduit`=:stock,`PrixProduit`=:prix,`ProduitEnPromotion`=:enpromo,`ReductionProduit`=:reduction,`Descriptif`=:description WHERE `IdProduit`=:id");
        $requeteModificationProduit->bindParam(':id', $id);
        $requeteModificationProduit->bindParam(':nom', $nom);
        $requeteModificationProduit->bindParam(':categorie', $categorie);
        $requeteModificationProduit->bindParam(':stock', $stock);
        $requeteModificationProduit->bindParam(':prix', $prix);
        $requeteModificationProduit->bindParam(':enpromo', $enPromotion);
        $requeteModificationProduit->bindParam(':reduction', $reduction);
        $requeteModificationProduit->bindParam(':description', $description);
        $requeteModificationProduit->execute();
        echo "<script type='text/javascript'> document.location.replace('index.php?uc=produit'); </script>";
    }

    //N'est pas utiliser ?
    public function supprnewprod($idprod)
    {
        $requeteModifierEffacerNouveauProduit = cb::$myPdo->prepare("UPDATE `produits` SET `NouveauProduit`=FALSE,`DteAfficheDebutNouveauProduit`=NULL,`DteAfficheFinNouveauProduit`=NULL WHERE `IdProduit`=:idprod");
        $requeteModifierEffacerNouveauProduit->bindParam(':idprod', $idprod);
        $requeteModifierEffacerNouveauProduit->execute();
    }

    public function modifnouveauproduit($nom, $datedebut, $datefin, $idproduit)
    {
        $requeteModifierNouveauProduit = cb::$myPdo->prepare("UPDATE `produits` SET `NomProduit`=:nom,`DteAfficheDebutNouveauProduit`=:datedebut,`DteAfficheFinNouveauProduit`=:datefin WHERE `IdProduit`=:idproduit");
        $requeteModifierNouveauProduit->bindParam(':nom', $nom);
        $requeteModifierNouveauProduit->bindParam(':datedebut', $datedebut);
        $requeteModifierNouveauProduit->bindParam(':datefin', $datefin);
        $requeteModifierNouveauProduit->bindParam(':idproduit', $idproduit);
        $requeteModifierNouveauProduit->execute();
        echo "<script type='text/javascript'> document.location.replace('index.php?uc=produit&co=nouveauteproduit'); </script>";
    }

    public function modifcategorieproduit($id, $nom, $Description)
    {
        $requeteModifierCategorieProduit = cb::$myPdo->prepare("UPDATE `categorieproduits` SET `NomCategorie`=:nom,`DescriptionCategorie`=:Description WHERE `IdCategorie`=:id");
        $requeteModifierCategorieProduit->bindParam(':id', $id);
        $requeteModifierCategorieProduit->bindParam(':nom', $nom);
        $requeteModifierCategorieProduit->bindParam(':Description', $Description);
        $requeteModifierCategorieProduit->execute();
        echo "<script type='text/javascript'> document.location.replace('index.php?uc=produit&co=categorieproduit'); </script>";
    }
}
