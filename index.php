<?php
session_start();
if ((!isset($_SESSION['modadmin'])) && (!isset($_SESSION['IdClient']))) {
    $_SESSION['modadmin'] = 'Visitor';
}
include("modeles/cbbdd.php");
$thePdo = cb::getPdocb();
?>
<!DOCTYPE html>

<html>
<head>
    <!-- //Main -->
    <meta name="description" content="Carbouffe-Web">
    <meta name="author" content="Kilian Mina - Léa Cotté">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="CarBouffe commercial site, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
              Smartphone Compatible web template"/>

    <!-- //Css -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/modern-business.css" rel="stylesheet">
    <link href="css/fontawesome.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/autocompletion.css" rel="stylesheet">
    <link rel="stylesheet" href="css/date.css"/>
    <link rel="stylesheet" href="css/date2.css"/>
    <link rel="stylesheet" href="css/payement.css"/>

    <!-- js -->
    <script type="text/javascript" src="js/confirmpass.js"></script>
    <script src="js/jq_scroll.js" type="text/javascript"></script>
    <script src="js/scroll.js" type="text/javascript"></script>


    <title>CarBouffe - Web</title>
</head>


<body>
<?php
//Header
if ($_SESSION['modadmin'] == 'Client') {
    include("vues/menu_co.html");
} else if (isset($_SESSION['modadmin'])) {
    include("vues/menu.html");
}

//echo password_hash("kilian", PASSWORD_BCRYPT);

//Récupérer menu
?>

<?php
//Récupére r UC
if (!isset($_REQUEST['uc']))
    $uc = 'accueil';
else
    $uc = $_REQUEST['uc'];

switch ($uc) {
    case 'accueil':
        {
            $resultprods = $thePdo->afficheRandomProd(6);
            $Lesnouveaute = $thePdo->affichenouveaute();
            include("vues/accueil.php");
            break;
        }
    case 'produits':
        {
            include("controllers/c_produits.php");
            break;
        }
    case 'offrespecial':
        {
            include("controllers/c_offrespecial.php");
            break;
        }
    case 'connect':
        {
            include("controllers/c_connect.php");
            break;
        }
    case 'contact':
        {
            include("controllers/c_contact.php");
            break;
        }
    case 'panier':
        {
            include("controllers/c_panier.php");
            break;
        }
    case 'profilclient':
        {
            include("controllers/c_profilclient.php");
            break;
        }

    case 'cgv':
        {
            include("vues/CGV.html");
            break;
        }
    case 'mentionslegales':
        {
            include("vues/mentionslegales.html");
            break;
        }
}

include("vues/popupdescription.php");
include("vues/popupproduct.php");
?>

<?php include("vues/footer.html"); ?>

<script type="text/javascript" src="js/autocompletion.js"></script>
<script src="js/login.js"></script>
<script src="js/popupprod.js"></script>
<script src="js/main.js"></script>
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/popper/popper.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jq_date.js"></script>
<script type="text/javascript" src="js/date.js"></script>
<script type="text/javascript" src="js/date2.js"></script>
<script src="js/payement.js" type="text/javascript"></script>


</body>
</html>
