# Carbouffe #

## Contexte ##
La société CarBouffe souhaite étendre son champ d’action en proposant un service de courses à domicile. 
Pour ce faire, elle souhaite mettre en place une application web permettant à tout habitant de la réunion de faire leurs 
courses sur le site CarBouffeWeb.

## Version ##

### V0.0 ###

Initialisation du projet.
Template : Bootstrap (Css)
Technologie mise en oeuvre : HTML / CSS / JavaScript / Php / Ajax / MySql / MVC / Variable de Session
Mission : mettre en place une application web permettant à tout habitant de la réunion de faire ses courses sur le site CarBouffeWeb et être ensuite livré
Développement : 
	* Réalisation de la prévision de gestion du groupe via un diagramme de Gantt
	* Réalisation des maquettes
	* Validation des maquettes
	* Développement de l’application

### V1.0 ###

Ajout de la partie Administrateur
Ajout de Fonts
Ajout d'une Backup image
Mise A Jour (Maj) de Bootstrap 

### V1.1 ###

Maj admin/
Maj modèle
Ajout d'Ajax

### V1.2 ###

Maj des variables de session
Maj Base de données 
Correction orthographique 

### V1.3 ###

Maj backup image
Correction de bogue niveau Controller / Vue / Css

### V1.4 ###

Résolution de bogue

Maj IMPORTANTE du modèle : 
    - Re nomage de toute la classe et ses fonctions (/modeles/cbbdd.php)
    - Suppression des fonctions doubles 
    - Modification de la gestion de la date (avoir 0+'X' quand l'heure/jour/minute/... < 0)

A Faire :
    -Fonction Ajouter Produit et Ajouter Nouveau Produit a fusioner 
    -Ajouter dans la Bdd une table ou un parametre disant que la commande est fini 